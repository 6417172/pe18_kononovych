let name = prompt("Name?");

while (!isNaN(name)) {
    name = prompt("Name?");
}

let age = Number(prompt("Age?"));
while (isNaN(age)) {
    age = Number(prompt("Age?"));
}
console.log(age);

console.log(isNaN(age));
console.log(!isNaN(name));
// ещё один вариант. он более короткий, но не очень корректный
/*while (isNaN(age) || !isNaN(name)) {
    name = prompt("Name?");
    age = Number(prompt("Age?"));
}*/

if (age < 18) {
    alert('You are not allowed to visit this website');
    // (age >= 18 && age <= 22) первый вариант, второй лучше
} else if (18 <= age && age <= 22) {
    const choice = confirm('Are you sure you want to continue?');
    console.log(choice);
    if (choice) {
        alert(`Welcome, ${name}!`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${name}!`)
}

// let num = prompt('введите число');
/*
function readNumber(num){
    if (num.isNaN) {
        prompt('это не число, попробуйте еще раз');
    } else {
        alert (num);
    }
}*/
