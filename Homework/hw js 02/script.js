let number = prompt('Введите любое число');

while ( number % 1 !==0 ) {
    number = prompt('Введите любое целое число');
}

if(number < 5) {
    console.log('Sorry, no number');
} else {
    for (let i = 0; i <= number; i++) {
        if (i % 5 === 0 ) {
            console.log(i);
        }
    }
    if (number % 5 !== 0 && number > 5) {
        console.log(number);
    }
}

