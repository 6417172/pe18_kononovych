const danItStudent = {
    name:"Роман",
    'last name':'Кононович',
    homework:30
}

console.log(danItStudent);

let property = prompt('Какое свойство хотите изменить?');
while (!(property in danItStudent)) {
    property = prompt(`${property} у объекта нет. Напишите, пожалуйста, правильное свойство объекта`);
}
let newValue = prompt('На какое значение?');


danItStudent[property] = newValue;
console.log(danItStudent);